package main

import (
	"backend/pkg/backgroundtasks"
	"backend/pkg/database"
	"backend/pkg/operations"
	"backend/pkg/reports"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
	"strconv"
)

var (
	ServerPort string
)

func init() {
	//logging configuration
	log.SetOutput(os.Stdout)
	log.SetFlags(log.Lshortfile)

	log.Println("main.go - initialization")

	// loading .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("error in loading the file", err)
	}

	//extracting data from env file
	Host := os.Getenv("host")
	Port, err := strconv.Atoi(os.Getenv("port"))
	if err != nil {
		log.Fatal("error in extracting port value from the file - converting from string to int/number", err)
	}
	User := os.Getenv("user")
	Password := os.Getenv("password")
	Dbname := os.Getenv("dbname")
	ServerPort = os.Getenv("server_port")
	IpAddress := os.Getenv("ip_address")
	MigrateDirectory := os.Getenv("migrate_directory")

	//initializing structs in database
	err = database.Initialization(Host, Port, User, Password, Dbname, MigrateDirectory, IpAddress)
	if err != nil {
		log.Fatal("error in initializing the database")
	}

	err = backgroundtasks.Initialization(os.Getenv("admin_email_address"), os.Getenv("admin_email_password"), os.Getenv("smtp_port"), os.Getenv("smtp_host"))
	if err != nil {
		log.Fatal("error in initializing the background tasks")
	}
}

func main() {
	log.Println("Main")

	// routing details
	router := mux.NewRouter()

	router.HandleFunc("/tasks", operations.GetAllTasks).Methods("GET")
	router.HandleFunc("/tasks", operations.CreateNewTask).Methods("POST")
	router.HandleFunc("/tasks/{task_id}", operations.DeleteTask).Methods("DELETE")
	router.HandleFunc("/tasks/{task_id}", operations.EditTask).Methods("PUT")

	router.HandleFunc("/reports/tasks-completion-report", reports.TasksCompletionReport).Methods("GET")
	router.HandleFunc("/reports/tasks-completion-per-day", reports.TasksCompletionPerDay).Methods("GET")
	router.HandleFunc("/reports/maximum-task-date", reports.MaximumTaskDate).Methods("GET")
	router.HandleFunc("/reports/task-not-completed-on-time", reports.TaskNotCompletedOnTime).Methods("GET")
	router.HandleFunc("/reports/tasks-open-per-day", reports.TasksOpenPerDay).Methods("GET")

	// serve the app
	log.Println("Server at ", ServerPort)
	log.Fatal(http.ListenAndServe(":"+ServerPort, router))
}
