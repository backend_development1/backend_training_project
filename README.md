# backend_training_project

## Getting started

Clone it and simply run it using following command
```
go run main.go
```

## URL Specifications
multiple ways of handling the URL
```
curl http://localhost:8080/get_all_tasks/munib.ahmed@emumba.com
```
here,
get_all_tasks is simply a path, it can be any string
munib.ahmed@emumba.com is simply a parameter provided, It can be carried into a variable and used. eg, in the below line, we are carrying it into address 
```
router.HandleFunc("/get_all_tasks/{address}", operations.View_All_Tasks).Methods("GET")
```


another way is 
```
curl http://localhost:8080/get_all_tasks --header "Content-Type: application/json" --request "GET" --data '{"address":"munib.ahmed@emumba.com"}'
```
here, data is the data provided. in the below line we didn't specified the adrress, code already knows it
```
router.HandleFunc("/get_all_tasks/", operations.View_All_Tasks).Methods("GET")
```