package models

import (
	"database/sql"
	"time"
)

type ReturnTaskInformation struct {
	TaskId           *int       `json:"task_id"`
	EmailAddress     *string    `json:"email_address"`
	Title            *string    `json:"title"`
	Description      *string    `json:"description"`
	CreationDate     *time.Time `json:"creation_date"`
	DueDate          *time.Time `json:"due_date"`
	CompletionDate   *time.Time `json:"completion_date"`
	CompletionStatus bool       `json:"completion_status"`
}

type GetTaskInformation struct {
	EmailAddress     *string    `json:"email_address"`
	Title            *string    `json:"title"`
	Description      *string    `json:"description"`
	DueDate          *time.Time `json:"due_date"`
	CompletionDate   *time.Time `json:"completion_date"`
	CompletionStatus bool
}

type DatabaseInformation struct {
	Host     string
	Port     int
	User     string
	Password string
	Dbname   string
	Db       *sql.DB
}

type TasksReport struct {
	TotalTasks     int `json:"total_tasks"`
	CompletedTasks int `json:"completed_tasks"`
	RemainingTasks int `json:"remaining_tasks"`
}

type EmailInformation struct {
	AdminEmailAddress  string
	AdminEmailPassword string
	SmtpHost           string
	SmtpPort           string
}
