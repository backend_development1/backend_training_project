package reports

import (
	"backend/pkg/database"
	"backend/pkg/models"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

const (
	Hours = 24
)

func TasksCompletionReport(w http.ResponseWriter, r *http.Request) {
	log.Println("TasksReport")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Address := r.URL.Query().Get("email_address")
	log.Println("email_address : ", Address)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	var Report models.TasksReport
	var err error
	Report, err = database.TasksCompletionReport(Address)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Report)
}

func TasksCompletionPerDay(w http.ResponseWriter, r *http.Request) {
	log.Println("TasksCompletionPerDay")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Address := r.URL.Query().Get("email_address")
	log.Println("email_address : ", Address)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	TaskCompleted, AccountCreationDate, err := database.TasksCompletionPerDay(Address)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	CurrentDate := time.Now()

	Difference := int(time.Now().Sub(AccountCreationDate).Hours()) / Hours
	var TasksCompletedPerDay int64
	TasksCompletedPerDay = int64(TaskCompleted / Difference)
	log.Println(TaskCompleted, AccountCreationDate, CurrentDate, TasksCompletedPerDay)

	json.NewEncoder(w).Encode(TasksCompletedPerDay)

	w.WriteHeader(http.StatusOK)
}

func TaskNotCompletedOnTime(w http.ResponseWriter, r *http.Request) {
	log.Println("TaskNotCompletedOnTime")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Address := r.URL.Query().Get("email_address")
	log.Println("email_address : ", Address)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	TaskCount, err := database.GetTasksNotCompletedOnTime(Address)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(TaskCount)
	w.WriteHeader(http.StatusOK)

}

func MaximumTaskDate(w http.ResponseWriter, r *http.Request) {
	log.Println("MaximumTasksPerDay")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Address := r.URL.Query().Get("email_address")
	log.Println("email_address : ", Address)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	MaxCompletedDate, err := database.GetMaximumTaskDate(Address)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(MaxCompletedDate)
	w.WriteHeader(http.StatusOK)
}

func TasksOpenPerDay(w http.ResponseWriter, r *http.Request) {
	log.Println("MaximumTasksPerDay")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Address := r.URL.Query().Get("email_address")
	log.Println("email_address : ", Address)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	MaxCompletedDate, err := database.GetTasksOpenPerDay(Address)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(MaxCompletedDate)
	w.WriteHeader(http.StatusOK)
}
