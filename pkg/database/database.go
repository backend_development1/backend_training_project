package database

import (
	"backend/pkg/models"
	"database/sql"
	"errors"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"log"
	"strconv"
	"time"
)

var (
	Database models.DatabaseInformation
)

// Initialization function
func Initialization(host string, port int, user string, password string, dbname string, migrateDirectory string, ip string) (err error) {
	log.Println("database.go - initialization")

	//setting up variables
	Database.Host = host
	Database.Port = port
	Database.User = user
	Database.Password = password
	Database.Dbname = dbname

	// connection string
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", Database.Host, Database.Port, Database.User, Database.Password, Database.Dbname)

	// open database
	Database.Db, err = sql.Open("postgres", psqlconn)
	if err != nil {
		log.Println("Error in setting up the database", err)
		return
	}

	err = MigrateDatabase(migrateDirectory, Database.User, Database.Password, ip, strconv.Itoa(Database.Port), Database.Dbname)
	return
}

// Migration Database function
func MigrateDatabase(migrateDirectory string, user string, password string, ip string, port string, dbname string) (err error) {
	m, err := migrate.New(
		"file:"+migrateDirectory,
		"postgres://"+user+":"+password+"@"+ip+":"+port+"/"+dbname+"?sslmode=disable")
	if err != nil {
		log.Println(err)
		return
	}
	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		log.Println(err)
	}
	return
}

func EditTask(Task models.GetTaskInformation, TaskId int) (TaskReturn models.ReturnTaskInformation, err error) {
	log.Println("EditTask")

	rows, err := Database.Db.Query("UPDATE tasks SET title=$1, description=$2, completion_date=$3, due_date=$4, completion_status=$5 WHERE task_id = $6 AND email_address = $7 RETURNING *", Task.Title, Task.Description, Task.CompletionDate, Task.DueDate, Task.CompletionStatus, TaskId, Task.EmailAddress)
	if err != nil {
		log.Println(err)
		return
	}

	TaskUpdate := false
	// Foreach iteration
	for rows.Next() {
		TaskUpdate = true
		err = rows.Scan(&TaskReturn.TaskId, &TaskReturn.EmailAddress, &TaskReturn.Title, &TaskReturn.Description, &TaskReturn.CompletionStatus, &TaskReturn.CreationDate, &TaskReturn.CompletionDate, &TaskReturn.DueDate)
		// check errors
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(TaskReturn.TaskId, TaskReturn.EmailAddress, TaskReturn.CreationDate, TaskReturn.Title, TaskReturn.Description, TaskReturn.CompletionDate, TaskReturn.DueDate, TaskReturn.CompletionStatus)
	}
	if TaskUpdate == false {
		err = errors.New("task_id is not corresponding to email_address - No task Updated")
		log.Println(err)
	}
	return
}

func CountAllTasks(EmailAddress string) (count int, err error) {
	log.Println("CountAllTasks")

	res, err := Database.Db.Query("SELECT count(*) FROM tasks WHERE email_address = $1", EmailAddress)

	for res.Next() {
		err = res.Scan(&count)
		if err != nil {
			log.Println(err)
		}
	}
	return
}

func CreateTask(Task models.GetTaskInformation) (TaskReturn models.ReturnTaskInformation, err error) {
	log.Println("CreateTask")
	var rows *sql.Rows

	//rows, err = Database.Db.Query("INSERT INTO tasks(email_address, creation_date, title, description, completion_date, due_date, completion_status) values ($1, $2, $3, $4, $5, $6, $7) RETURNING *", Task.EmailAddress, Task.CreationDate, Task.Title, Task.Description, Task.CompletionDate, Task.DueDate, Task.CompletionStatus)
	rows, err = Database.Db.Query("INSERT INTO tasks(email_address, creation_date, title, description, completion_date, due_date, completion_status) values ($1, NOW()::timestamp, $2, $3, $4, $5, $6) RETURNING *", Task.EmailAddress, Task.Title, Task.Description, Task.CompletionDate, Task.DueDate, Task.CompletionStatus)

	// check errors
	if err != nil {
		log.Println(err)
		return
	}

	// Foreach iteration
	for rows.Next() {
		err = rows.Scan(&TaskReturn.TaskId, &TaskReturn.EmailAddress, &TaskReturn.Title, &TaskReturn.Description, &TaskReturn.CompletionStatus, &TaskReturn.CreationDate, &TaskReturn.CompletionDate, &TaskReturn.DueDate)
		// check errors
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(TaskReturn.TaskId, TaskReturn.EmailAddress, TaskReturn.CreationDate, TaskReturn.Title, TaskReturn.Description, TaskReturn.CompletionDate, TaskReturn.DueDate, TaskReturn.CompletionStatus)
	}
	return
}

func GetAllTasks(EmailAddress string, Page int, Limit int) (Tasks []models.ReturnTaskInformation, err error) {
	log.Println("GetAllTasks")

	var offset int = (Page - 1) * Limit
	rows, err := Database.Db.Query("SELECT * FROM tasks WHERE email_address = $1 LIMIT $2 OFFSET $3", EmailAddress, Limit, offset)

	// check errors
	if err != nil {
		log.Println(err)
		return
	}

	// Foreach iteration
	for rows.Next() {
		var Task models.ReturnTaskInformation
		err = rows.Scan(&Task.TaskId, &Task.EmailAddress, &Task.Title, &Task.Description, &Task.CompletionStatus, &Task.CreationDate, &Task.CompletionDate, &Task.DueDate)
		// check errors
		if err != nil {
			log.Println(err)
			return
		}
		Tasks = append(Tasks, Task)
	}
	return
}

func DeleteTasks(EmailAddress string, TaskId int) (Task models.ReturnTaskInformation, err error) {
	log.Println("DeleteTasks")

	rows, err := Database.Db.Query("DELETE FROM tasks WHERE email_address = $1 and task_id=$2 RETURNING *", EmailAddress, TaskId)
	if err != nil {
		log.Println(err)
		return
	}

	TaskUpdate := false
	// Foreach iteration
	for rows.Next() {
		TaskUpdate = true
		err = rows.Scan(&Task.TaskId, &Task.EmailAddress, &Task.Title, &Task.Description, &Task.CompletionStatus, &Task.CreationDate, &Task.CompletionDate, &Task.DueDate)
		// check errors
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(Task.TaskId, Task.EmailAddress, Task.CreationDate, Task.Title, Task.Description, Task.CompletionDate, Task.DueDate, Task.CompletionStatus)
	}
	if TaskUpdate == false {
		err = errors.New("task_id is not corresponding to email_address - No task Deleted/Updated")
		log.Println(err)
	}
	return
}

func GetTasksNotCompletedOnTime(EmailAddress string) (TaskCount int, err error) {
	log.Println("GetTasksNotCompletedOnTime:", EmailAddress)

	rows, err := Database.Db.Query("SELECT COUNT(*) FROM tasks WHERE email_address = $1 AND completion_status = true AND completion_date <= due_date", EmailAddress)
	if err != nil {
		log.Println(err)
		return
	}

	// Foreach iteration
	for rows.Next() {
		err = rows.Scan(&TaskCount)
		// check errors
		if err != nil {
			log.Println(err)
		}
	}
	return
}

func GetMaximumTaskDate(EmailAddress string) (TaskDate time.Time, err error) {
	log.Println("GetAccountCreationDate:", EmailAddress)

	rows, err := Database.Db.Query("SELECT max(completion_date) FROM tasks WHERE email_address = $1 AND completion_status = true", EmailAddress)
	if err != nil {
		log.Println(err)
		return
	}

	// Foreach iteration
	for rows.Next() {
		err = rows.Scan(&TaskDate)
		// check errors
		if err != nil {
			log.Println(err)
		}
	}
	return
}

func GetTasksOpenPerDay(EmailAddress string) (MyMap map[string]int, err error) {
	log.Println("GetAccountCreationDate:", EmailAddress)

	rows, err := Database.Db.Query("SELECT d.day, COUNT(d.day) FROM (SELECT TO_CHAR(creation_date, 'Day') as day FROM tasks WHERE email_address=$1) as d GROUP BY d.day", EmailAddress)
	if err != nil {
		log.Println(err)
		return
	}

	MyMap = make(map[string]int)
	var NumberOfTasks int
	var Days string

	// Foreach iteration
	for rows.Next() {
		err = rows.Scan(&Days, &NumberOfTasks)
		// check errors
		if err != nil {
			log.Println(err)
		}
		MyMap[Days] = NumberOfTasks
	}
	return
}

func TasksCompletionReport(EmailAddress string) (Report models.TasksReport, err error) {
	log.Println("TasksCompletionReport: ", EmailAddress)

	res, err := Database.Db.Query("SELECT count(email_address), sum(case completion_status when 'true' then 1 else 0 end), sum(case completion_status when 'false' then 1 else 0 end) FROM tasks WHERE email_address = $1", EmailAddress)

	// check errors
	if err != nil {
		log.Println(err)
		return
	}

	for res.Next() {
		err = res.Scan(&Report.TotalTasks, &Report.CompletedTasks, &Report.RemainingTasks)
		if err != nil {
			log.Println(err)
		}
	}
	return
}

func TasksCompletionPerDay(EmailAddress string) (TaskCompleted int, AccountCreationDate time.Time, err error) {
	log.Println("TasksCompletionPerDay :", EmailAddress)

	res, err := Database.Db.Query("SELECT count(tasks.email_address), users.account_creation_date FROM tasks, users WHERE completion_status = true AND tasks.email_address=$1 AND tasks.email_address=users.email_address Group by users.account_creation_date", EmailAddress)

	for res.Next() {
		err = res.Scan(&TaskCompleted, &AccountCreationDate)
		if err != nil {
			log.Println(err)
		}
	}
	return
}

func SendAlertsForTodayDueTask() (Tasks []models.ReturnTaskInformation, err error) {
	log.Println("TasksDueToday")

	rows, err := Database.Db.Query("SELECT * FROM tasks WHERE due_date = NOW()::date ORDER BY email_address")

	// check errors
	if err != nil {
		return
	}

	// Foreach iteration
	for rows.Next() {
		var Task models.ReturnTaskInformation
		err = rows.Scan(&Task.TaskId, &Task.EmailAddress, &Task.Title, &Task.Description, &Task.CompletionStatus, &Task.CreationDate, &Task.CompletionDate, &Task.DueDate)
		// check errors
		if err != nil {
			return
		}
		Tasks = append(Tasks, Task)
	}
	return
}
