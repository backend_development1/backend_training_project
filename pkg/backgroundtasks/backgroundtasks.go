package backgroundtasks

import (
	"backend/pkg/database"
	"backend/pkg/models"
	"github.com/robfig/cron"
	"log"
	"net/smtp"
	"strconv"
	"time"
)

var (
	backgroundTask models.EmailInformation
)

// Initialization function
func Initialization(adminEmailAddress string, adminEmailPassword string, smtpPort string, smtpHost string) (err error) {
	backgroundTask.AdminEmailAddress = adminEmailAddress
	backgroundTask.AdminEmailPassword = adminEmailPassword
	backgroundTask.SmtpPort = smtpPort
	backgroundTask.SmtpHost = smtpHost

	var l *time.Location
	l, err = time.LoadLocation("UTC")
	if err != nil {
		return
	}
	c := cron.NewWithLocation(l)
	c.AddFunc("@midnight", SendAlertsForTodayDueTask)
	c.Start()
	return
}

func email(taskList []models.ReturnTaskInformation, emailAddress string) (err error) {
	// Message
	strMessage := "List of Tasks Due Today \n-------------------------------------------------------------------------- \n"

	for _, task := range taskList {
		strMessage = strMessage + "TaskID: " + strconv.Itoa(*task.TaskId) + "\n"
		strMessage = strMessage + "Email Address: " + (*task.EmailAddress) + "\n"
		strMessage = strMessage + "Title: " + (*task.Title) + "\n"
		strMessage = strMessage + "Description: " + (*task.Description) + "\n"
		strMessage = strMessage + "Completion status: " + strconv.FormatBool(task.CompletionStatus) + "\n"
		strMessage = strMessage + "Creation Date: " + (*task.CreationDate).String() + "\n"
		strMessage = strMessage + "Completion Date: " + (*task.CompletionDate).String() + "\n"
		strMessage = strMessage + "Due Date: " + (*task.DueDate).String() + "\n \n"
	}
	strMessage = strMessage + "-------------------------------------------------------------------------- \n"

	//byte conversion
	log.Println(strMessage)

	// Authentication.
	auth := smtp.PlainAuth("", backgroundTask.AdminEmailAddress, backgroundTask.AdminEmailPassword, backgroundTask.SmtpHost)

	// Sending email.
	err = smtp.SendMail(backgroundTask.SmtpHost+":"+backgroundTask.SmtpPort, auth, backgroundTask.AdminEmailAddress, []string{emailAddress}, []byte(strMessage))
	if err != nil {
		return
	}
	return
}

func SendAlertsForTodayDueTask() {
	log.Println("Executing bg task: SendAlertsForTodayDueTask")
	allTasksList, err := database.SendAlertsForTodayDueTask()

	if err != nil {
		log.Println(err.Error())
		return
	}
	log.Println("Sending email to all users for their due tasks today", time.Now().UTC().Format("2006-01-02"), "12:00 AM UTC")

	var emailAddress string
	var startingIndex int
	// iterating over the slice of tasks
	// slice is sorted on the bases of email address
	for i, task := range allTasksList {
		// initializing the email address variable, for first iteration
		if i == 0 {
			emailAddress = *task.EmailAddress
		} else if emailAddress != *task.EmailAddress {
			// if email address variable changes from the tasks.email_address
			// it means we have all the tasks for one email address.
			// Thus ready to generate the email to the corresponding email address
			err = email(allTasksList[startingIndex:i], emailAddress)
			if err != nil {
				log.Println(err.Error())
			}

			// logging and updating variable for the next iteration/ email address
			log.Println(*task.EmailAddress)
			startingIndex = i
			emailAddress = *task.EmailAddress
		}
	}

	// generating the email for the last email address
	err = email(allTasksList[startingIndex:len(allTasksList)], emailAddress)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println("Sent email to all users for their due tasks today")
}
