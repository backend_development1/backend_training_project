package operations

import (
	"backend/pkg/database"
	"backend/pkg/models"
	"encoding/json"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"strconv"
)

const (
	DefaultLimit int = 10
	DefaultPage  int = 1
)

func CreateNewTask(w http.ResponseWriter, r *http.Request) {
	log.Println("CreateNewTask")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	var Task models.GetTaskInformation
	err := json.NewDecoder(r.Body).Decode(&Task)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Println(Task, Task.EmailAddress)
	if Task.EmailAddress == nil || len(*Task.EmailAddress) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	// counting number of tasks with corresponding to the email address
	var TaskCount int
	TaskCount, err = database.CountAllTasks(*Task.EmailAddress)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if TaskCount < 50 {
		if Task.CompletionDate != nil && !Task.CompletionDate.IsZero() && Task.DueDate != nil && !Task.DueDate.IsZero() {
			Task.CompletionStatus = true
		} else {
			Task.CompletionStatus = false
		}
		// calling database function
		var TaskReturn models.ReturnTaskInformation
		TaskReturn, err = database.CreateTask(Task)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		//taking data to return
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(TaskReturn)
		return
	}
	http.Error(w, "Something went wrong - either title provided is not unique or tasks count is more the tasks limit", http.StatusBadRequest)
	return
}

func EditTask(w http.ResponseWriter, r *http.Request) {
	log.Println("EditTask")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	var Task models.GetTaskInformation
	err := json.NewDecoder(r.Body).Decode(&Task)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	log.Println(Task.EmailAddress)

	Vars := mux.Vars(r)
	TaskId, err := strconv.Atoi(Vars["task_id"])
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if Task.EmailAddress == nil || len(*Task.EmailAddress) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}
	if Task.CompletionDate != nil && !Task.CompletionDate.IsZero() && Task.DueDate != nil && !Task.DueDate.IsZero() {
		Task.CompletionStatus = true
	} else {
		Task.CompletionStatus = false
	}

	//calling database functions
	TaskReturn, err := database.EditTask(Task, TaskId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(TaskReturn)
}

func DeleteTask(w http.ResponseWriter, r *http.Request) {
	log.Println("DeleteTask")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Vars := mux.Vars(r)
	TaskId, err := strconv.Atoi(Vars["task_id"])
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	Address := r.URL.Query().Get("email_address")
	log.Println(Vars, TaskId)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	//calling database functions
	Task, err := database.DeleteTasks(Address, TaskId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Task)
	return
}

func GetAllTasks(w http.ResponseWriter, r *http.Request) {
	log.Println("GetAllTasks")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	Address := r.URL.Query().Get("email_address")
	log.Println("email_address : ", Address)
	if len(Address) == 0 {
		http.Error(w, "Email Address field is not provided/Empty", http.StatusBadRequest)
		return
	}

	var Page int
	var err error
	strPage := r.URL.Query().Get("page")
	if strPage == "" {
		Page = DefaultPage
	} else {
		Page, err = strconv.Atoi(strPage)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	var Limit int
	strLimit := r.URL.Query().Get("limit")
	if strLimit == "" {
		Limit = DefaultLimit
	} else {
		Limit, err = strconv.Atoi(strLimit)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
	log.Println("Page: ", Page, "Limit: ", Limit)

	//calling database functions
	Tasks, err := database.GetAllTasks(Address, Page, Limit)

	// check errors
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(Tasks)
	return
}

func AttachFile(w http.ResponseWriter, r *http.Request) {
	log.Println("AttachFile")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	FilePath := r.URL.Query().Get("file_path")
	log.Println("file_path : ", FilePath)
	if len(FilePath) == 0 {
		http.Error(w, "file_path field is not provided/Empty", http.StatusBadRequest)
	}

}

func DownloadFile(w http.ResponseWriter, r *http.Request) {
	log.Println("DownloadFile")
	w.Header().Set("Content-Type", "application/json")

	//extracting data from request
	FilePath := r.URL.Query().Get("file_path")
	log.Println("file_path : ", FilePath)
	if len(FilePath) == 0 {
		http.Error(w, "file_path field is not provided/Empty", http.StatusBadRequest)
	}
}
