ALTER TABLE users ALTER COLUMN account_creation_date TYPE date;
ALTER TABLE tasks ALTER COLUMN creation_date TYPE date;
ALTER TABLE tasks ALTER COLUMN due_date TYPE date;
ALTER TABLE tasks ALTER COLUMN completion_date TYPE date;


ALTER TABLE tasks ALTER COLUMN task_id TYPE BIGINT;
ALTER TABLE tasks ALTER COLUMN description TYPE VARCHAR(50);
ALTER TABLE tasks ALTER COLUMN completion_date SET NOT NULL;