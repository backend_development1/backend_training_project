ALTER TABLE users ALTER COLUMN account_creation_date TYPE timestamp;

ALTER TABLE tasks ALTER COLUMN creation_date TYPE timestamp;
ALTER TABLE tasks ALTER COLUMN completion_date TYPE timestamp;
ALTER TABLE tasks ALTER COLUMN due_date TYPE timestamp;
ALTER TABLE tasks ALTER COLUMN description TYPE VARCHAR(256);
ALTER TABLE tasks ALTER COLUMN completion_date DROP NOT NULL;