CREATE TABLE users (
                       account_creation_date date,
                       email_address VARCHAR(50) NOT NULL
);
CREATE TABLE tasks (
                           task_id SERIAL PRIMARY KEY,
                           email_address VARCHAR(50) NOT NULL,
                           title VARCHAR(50) NOT NULL,
                           description VARCHAR(50) NOT NULL,
                           completion_status boolean NOT NULL,
                           creation_date DATE NOT NULL,
                           completion_date DATE NOT NULL,
                           due_date DATE NOT NULL
);
